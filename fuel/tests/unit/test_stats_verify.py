from datetime import datetime

from django.test import TestCase

from fuel.models.fuel import Fuel
from fuel.stats import Stats


class TestStatsVerify(TestCase):
    """Test that verify system works."""

    def test_correct(self):
        """Correct stats."""
        Fuel(date=datetime(2020, 1, 1), milage=10000, fuel_tanked=0, money=1, liter_price=1).save()
        Fuel(date=datetime(2021, 5, 5), milage=10100, fuel_tanked=2, money=1, liter_price=1).save()

        result = Stats().verify()

        self.assertEqual(result, [])

    def test_incorrect(self):
        """Incorrect stats."""
        Fuel(date=datetime(2020, 1, 1), milage=1000, fuel_tanked=1, money=1, liter_price=1).save()
        Fuel(date=datetime(2021, 5, 5), milage=100, fuel_tanked=1, money=1, liter_price=1).save()

        result = Stats().verify()

        self.assertEqual(result, [1])

