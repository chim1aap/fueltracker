from fuel.models.fuel import Fuel
from django.test import TestCase


class FuelVerifyTestCase(TestCase):

    def test_valid_entry_valid(self):
        """Valid entry."""
        f = Fuel(fuel_tanked=100, money=100, liter_price=1)
        assert f.valid_entry

    def test_valid_entry_invalid(self):
        """Invalid entry."""
        f = Fuel(fuel_tanked=100, money=100, liter_price=2)
        assert not f.valid_entry

    def test_valid_entry_nearly_correct(self):
        """valid entry is correct but there is some rounding error."""
        f = Fuel(fuel_tanked=12.35, money=12.35, liter_price=1.01)
        assert f.valid_entry
