from datetime import datetime

from django.test import TestCase
from decimal import Decimal

from fuel.models.fuel import Fuel
from fuel.models.other import Other
from fuel.stats import Stats


class StatsTestCase(TestCase):
    """Test the small functions."""

    def setUp(self):

        Fuel(date=datetime(2020, 1, 1), money=11.11, liter_price=1, milage=10000, fuel_tanked=0).save()
        Fuel(date=datetime(2021, 5, 5), money=22.22, liter_price=1, milage=10100, fuel_tanked=2).save()
        Fuel(date=datetime(2022, 7, 5), money=66.66, liter_price=1, milage=10200, fuel_tanked=2).save()
        Other(date=datetime(2020, 1, 1), recurrence=Other.Recurrence.Never, recurrence_iteration_amount=12, money=1000.00).save()
        Other(date=datetime(2020, 1, 2), recurrence=Other.Recurrence.Daily, recurrence_iteration_amount=2, money=3.00).save()
        Other(date=datetime(2020, 1, 3), recurrence=Other.Recurrence.Weekly, recurrence_iteration_amount=10, money=5.00).save()
        Other(date=datetime(2020, 1, 4), recurrence=Other.Recurrence.Monthly, recurrence_iteration_amount=12, money=7.00).save()
        Other(date=datetime(2020, 1, 5), recurrence=Other.Recurrence.Yearly, recurrence_iteration_amount=12, money=11.00).save()

    def test_efficiency(self):
        result = Stats().efficiency()

        self.assertEqual(result, Decimal(2.0))

    def test_sum(self):
        result = Stats().sum_money()

        self.assertEqual(result, Decimal('99.99'))

    def test_summary(self):
        result = Stats().format().summary()

        self.assertEqual(result['fuel']['total'], '€ 99.99')
        # 1000 + 2*3 + 10*5 + 12 * 7 + 12*11
        self.assertEqual(result['other']['total'], '€ 1272.00')
        self.assertEqual(result['total']['total'], '€ 1371.99')
