from gettext import gettext

from django.contrib import admin
from django.contrib.admin import AdminSite

from fuel.models.other import Other
from fuel.models.fuel import Fuel


# Register your models here - This caused them to appear in the admin page.

class FuelAdmin(admin.ModelAdmin):
    """Admin representation of the fuel table."""
    fieldsets = [
        (None, {
            "fields": [
                "date",
                "fuel_tanked",
                "milage",
                "money"
            ]}),
        ("Additional", {
            "fields": [
                "liter_price",
                "comments",
                "has_refueled_full"
            ]
        })
    ]

    list_display = ["id", "milage", "date", "fuel_tanked", "money", "has_refueled_full"]
    list_filter = ["has_refueled_full", "date", "date_added"]
    ordering = ["-date"]


class ExpensesAdmin(admin.ModelAdmin):
    """Admin representation of the expenses table."""
    list_display = ["name", "date", "money", "recurrence", "recurrence_iteration_amount"]
    list_filter = ["recurrence", "date"]
    ordering = ["-date"]


class FueltrackerAdminSite(AdminSite):
    """
    Customizes look and feel of the Admin interface for Fueltracker.
    """
    site_header = gettext("Fueltracker Admin")
    site_title = gettext("Fueltracker site Admin")


# Register Admin pages.
admin_site = FueltrackerAdminSite(name="FuelAdmin")
admin_site.register(Fuel, FuelAdmin)
admin_site.register(Other, ExpensesAdmin)
