import csv
from datetime import datetime

from django.core.exceptions import PermissionDenied
from django.http import HttpResponse

from fuel.models.fuel import Fuel


# Imports a CSV file
with open("./Benzinedata.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            _, created = Fuel.objects.get_or_create(
                date_refueled=datetime.strptime(row[1], "%Y-%m-%d"),
                milage=row[2],
                fuel_tanked=row[3],
                money=row[4],
                liter_price=row[5],
                comments=row[6],
                has_refueled_full=row[7] if row[7] else True
            )
        except Exception as e:
            print(e)


class CSV:

    @staticmethod
    def download_csv(modeladmin, request, queryset):
        if not request.user.is_staff:
            raise PermissionDenied
        opts = queryset.model._meta
        model = queryset.model
        response = HttpResponse(mimetype='text/csv')
        # force download.
        response['Content-Disposition'] = 'attachment;filename=export.csv'
        # the csv writer
        writer = csv.writer(response)
        field_names = [field.name for field in opts.fields]
        # Write a first row with header information
        writer.writerow(field_names)
        # Write data rows
        for obj in queryset:
            writer.writerow([getattr(obj, field) for field in field_names])
        return response

    download_csv.short_description = "Download selected as csv"