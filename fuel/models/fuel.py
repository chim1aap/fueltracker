from django.db import models
from django.db.models import Max, Min, Sum
from django.utils.translation import gettext

from fuel.models.Manager import Manager


class FuelManager(Manager):
    """
    Fuel Table Manager. This adds table level functionality.
    """

    @property
    def distance(self) -> int:
        """The time between the first and the last time."""
        return self.aggregate(Max('milage'))['milage__max'] - self.aggregate(Min('milage'))['milage__min']

    @property
    def total_money(self):
        """The total money spend."""
        return self.aggregate(Sum('money'))['money__sum']

    @property
    def total(self):
        """Alias for manager."""
        return self.total_money

    def total_fuel(self):
        """The total amount of fuel used."""
        return self.aggregate(Sum('fuel_tanked'))['fuel_tanked__sum']


class Fuel(models.Model):
    """
    Represents the fuel table.

    This contains all the records of fueling up.
    The methods are on an instance or row.
    """
    # Note: The
    objects = FuelManager()

    date = models.DateField(gettext('Date refueled'),
                            help_text=gettext("Date when you refueled."))

    milage = models.IntegerField(gettext('milage'), help_text=gettext("The milage of the car."))

    fuel_tanked = models.DecimalField(gettext('Fuel Tanked'), max_digits=6, decimal_places=2,
                                      help_text=gettext("The amount of fuel that went into the car."""))

    money = models.DecimalField(gettext('Money'), max_digits=6, decimal_places=2,
                                help_text=gettext("The amount of money you had to pay."))

    liter_price = models.DecimalField(gettext('Price/Liter'), max_digits=6, decimal_places=4,
                                      help_text=gettext("The price per liter when you refueled."))

    comments = models.TextField(gettext('Comments'), blank=True,
                                help_text=gettext("Any comments on this entry."""))

    has_refueled_full = models.BooleanField(gettext('has_refueled_full'), default=True, help_text=gettext(
        "Whether the fuel tank of the car was completely filled up"))

    date_added = models.DateTimeField(gettext('date_added'), auto_now=True,
                                      help_text=gettext("Date the entry was added to the database."))

    @property
    def valid_entry(self) -> bool:
        """Returns if the `money`, `liter_price` and `fuel_tanked` are roughly correct."""
        return -0.10 < self.money / self.fuel_tanked - self.liter_price < 0.10

    def __str__(self):
        ### To String, is also used by various parts in the django admin ###
        s = str(self.id)
        s += " " + str(self.date)
        s += " " + str(self.fuel_tanked)
        return s
