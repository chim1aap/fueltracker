from datetime import timedelta
from decimal import Decimal

from django.db import models
from django.db.models import Max, Min


class Manager(models.Manager):
    """Manager containing functions defined on both models."""
    def summary(self):
        """Returns a summary object."""
        return {
            'total': Decimal(self.total).quantize(Decimal('.01')),
            'daily': Decimal(self.total / Decimal(self.timespan.days)).quantize(Decimal('.01')),
            'weekly': Decimal(self.total / Decimal(self.timespan.days / 7)).quantize(Decimal('.01')),
            'monthly': Decimal(self.total / Decimal(self.timespan.days / 30)).quantize(Decimal('.01')),
            'yearly': Decimal(self.total / Decimal(self.timespan.days / 365)).quantize(Decimal('.01')),
        }

    @property
    def timespan(self) -> timedelta:
        """The time between the first and the last time."""
        return self.aggregate(Max('date'))['date__max'] - self.aggregate(Min('date'))[
            'date__min']
