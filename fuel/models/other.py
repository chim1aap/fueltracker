from datetime import timedelta

from _decimal import Decimal
from django.db import models
from django.db.models import Sum, F, Max, Min
from django.utils.translation import gettext_lazy, gettext

from fuel.models.Manager import Manager


class OtherManager(Manager):
    """
    OtherExpenses Manager providing table wide functionality.
    """

    @property
    def total(self) -> Decimal:
        return self.aggregate(total=Sum(F('money') * F('recurrence_iteration_amount')))['total']


class Other(models.Model):
    """
    Represents the other expenses table.

    These are trips to the garage, the insurance and other items which happen whether the car runs or not.
    """
    objects = OtherManager()

    class Recurrence(models.TextChoices):
        """Interval of the recurring expenses."""
        Never = "N", gettext_lazy("Never")
        Daily = "D", gettext_lazy("Daily")
        Weekly = "W", gettext_lazy("Weekly")
        Monthly = "M", gettext_lazy("Monthly")
        Yearly = "Y", gettext_lazy("Yearly")

    name = models.CharField(gettext('Name'), max_length=120, help_text=gettext("Name of the expense."))

    date = models.DateField(gettext('Date of expense'), help_text=gettext("The date when the expense was made."))

    money = models.DecimalField(gettext('Money'), max_digits=6, decimal_places=2,
                                help_text=gettext("The amount of money of the expense."))

    date_added = models.DateTimeField(gettext('date_added'), auto_now=True,
                                      help_text=gettext("Date the entry was added to the database."))

    recurrence = models.CharField(max_length=1,
                                  choices=Recurrence.choices,
                                  help_text=gettext("Interval of the recurring expense."),
                                  default=Recurrence.Never)
    recurrence_iteration_amount = models.IntegerField(gettext("Recurrence iteration amount"),
                                                      help_text=gettext(
                                                          "The amount of times this expense has occurred or "
                                                          "will occur."
                                                          "The final occurrence may be in future."),
                                                      default=1)
    comments = models.TextField(gettext('Comments'), blank=True,
                                help_text=gettext("Any comments on this entry."))
