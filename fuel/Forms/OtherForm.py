from django import forms

from fuel.models.other import Other


class OtherForm(forms.ModelForm):
    """Form to add a new entry to the other table."""

    class Meta:
        model = Other
        fields = [
            "name",
            "date",
            "money",
            "recurrence",
            "recurrence_iteration_amount",
            "comments"
        ]
