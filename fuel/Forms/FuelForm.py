from django import forms

from fuel.models.fuel import Fuel


class FuelForm(forms.ModelForm):
    """Form to add a new entry to the fuel table."""
    class Meta:
        model = Fuel
        fields = [
            "date",
            "money",
            "fuel_tanked",
            "milage",
            "liter_price",
            "has_refueled_full",
            "comments",
        ]
