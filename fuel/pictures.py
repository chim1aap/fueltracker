import io

from django.http import HttpResponse
from pandas import DataFrame
from plotnine import *
from plotnine.themes.themeable import legend_text, legend_position

from fuel.models.fuel import Fuel
import matplotlib

from fuel.stats import Stats


class Singleton(type):
    """Singleton metaclass."""
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class PicturesView:
    """Static view response class for pictures"""

    @staticmethod
    def trend(request) -> HttpResponse:
        return Pictures.build_picture(Pictures().trend())

    @staticmethod
    def monthly(request) -> HttpResponse:
        return Pictures.build_picture(Pictures().month_expenses())

    @staticmethod
    def money(request) -> HttpResponse:
        return Pictures.build_picture(Pictures().money())

    @staticmethod
    def other(request) -> HttpResponse:
        return Pictures.build_picture(Pictures().other_expenses())


class Pictures(metaclass=Singleton):
    """Class to generate the pretty pictures."""

    def __init__(self):
        matplotlib.use('Agg')  # sets the backend to not use a GUI.
        self.df = DataFrame.from_records(Fuel.objects.all().values())
        self.stats = Stats().format()

        # Casting as correct values.
        self.df['money'] = self.df.money.astype('float')
        self.df['milage'] = self.df.milage.astype('int')

        self.stats.combined['money'] = self.stats.combined['money'].astype('float')
        self.df['date'] = self.df['date'].astype('datetime64[ns]')

    def summary(self):
        return self.df.dtypes

    @staticmethod
    def build_picture(picture: ggplot):
        buffer = io.BytesIO()
        picture.save(buffer, 'svg')
        return HttpResponse(buffer.getvalue(), content_type='image/svg+xml')

    def other_expenses(self):
        return (ggplot(self.stats.expenses_extended, aes("name", "money", fill="factor(date)"))
                + geom_col(show_legend=False)
                )

    def trend(self) -> ggplot:
        return (ggplot(self.df, aes("date", "milage"))
                + geom_point()
                + stat_smooth(method="lm")
                + scale_x_date()
                )

    def money(self) -> ggplot:
        return (ggplot(self.df, aes("milage", "money"))
                + geom_point()
                + stat_smooth(method="lm")
                )

    def month_expenses(self):
        return (ggplot(self.stats.combined, aes("monthly", "money", fill="name"))
                + geom_col()
                ) + theme(
            legend_position="top",
            legend_title=element_blank(),
            axis_text_x=element_text(rotation=90)
        )
