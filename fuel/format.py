from decimal import Decimal


def format_money(money: Decimal) -> str:
    """Format any money."""
    return f"€ {money.quantize(Decimal('.01'))}"


def format_liter(liter: Decimal) -> str:
    return f"{liter.quantize(Decimal('.01'))} L"


def format_liter_price(liter: Decimal) -> str:
    return f"{liter.quantize(Decimal('.01'))} €/L"
