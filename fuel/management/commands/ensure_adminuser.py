import os
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


# Source: https://stackoverflow.com/questions/39744593/how-to-create-a-django-superuser-if-it-doesnt-exist-non-interactively
class Command(BaseCommand):
    help = "Creates an admin user non-interactively if it doesn't exist"

    def add_arguments(self, parser):
        parser.add_argument('--username', help="Admin's username")
        parser.add_argument('--email', help="Admin's email")
        parser.add_argument('--password', help="Admin's password")
        parser.add_argument('--no-input', help="Read options from the environment",
                            action='store_true')

    def handle(self, *args, **options) -> str:
        """Returns whether a superuser is created (As a string, not a ."""
        User = get_user_model()

        if options['no_input']:
            # Read from environment variables.
            options['username'] = os.getenv('DJANGO_SUPERUSER_USERNAME', "admin")
            options['email'] = os.getenv('DJANGO_SUPERUSER_EMAIL', "admin")
            options['password'] = os.getenv('DJANGO_SUPERUSER_PASSWORD', "admin")

        exists = User.objects.filter(username=options['username']).exists()
        if not exists:
            User.objects.create_superuser(username=options['username'],
                                          email=options['email'],
                                          password=options['password'])
        return str(not exists)
