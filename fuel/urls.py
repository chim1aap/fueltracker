from django.urls import path

from . import views
from .pictures import PicturesView

urlpatterns = [
    path('', views.index, name='index'),
    path('image/trend.svg', PicturesView.trend, name='trend'),
    path('image/money.svg', PicturesView.money, name='money'),
    path('image/other.svg', PicturesView.other, name='other'),
    path('image/monthly.svg', PicturesView.monthly, name='monthly'),
    path('export/fuel.csv', views.export, name='export'),
    path('export/other.csv', views.export_other, name='export_other'),
    path('add-fuel', views.add, name='add'),
    path('add-other', views.add_others, name='other')
]
