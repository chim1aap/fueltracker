from pandas import DataFrame, DateOffset, Timestamp, concat
from decimal import Decimal

from fuel.format import format_money
from fuel.models.fuel import Fuel
from fuel.models.other import Other


class Stats:
    """Calculates some statistics about the fuel table."""

    def __init__(self):
        self.fuel: DataFrame = DataFrame.from_records(Fuel.objects.all().values())
        self.expenses: DataFrame = DataFrame.from_records(Other.objects.all().values())
        self.expenses_extended: DataFrame
        self.combined: DataFrame

    def summary(self):
        sum_fuel = self.fuel["money"].sum()
        sum_other = self.expenses_extended["money"].sum()
        refueled__max = self.fuel["date"].max()
        refueled__min = self.fuel["date"].min()
        expensed__max = self.expenses_extended["date"].max()
        expensed__min = self.expenses_extended["date"].min()
        timespan_fuel = refueled__max - refueled__min
        timespan_other = expensed__max - expensed__min
        sum_total = sum_other + sum_fuel
        timespan_total = max(expensed__max, refueled__max) - min(expensed__min, refueled__min)
        return {
            'fuel': {
                'total': format_money(Decimal(sum_fuel)),
                'daily': format_money(Decimal(sum_fuel / Decimal(timespan_fuel.days))),
                'weekly': format_money(Decimal(sum_fuel / Decimal(timespan_fuel.days / 7))),
                'monthly': format_money(Decimal(sum_fuel / Decimal(timespan_fuel.days / 30))),
                'yearly': format_money(Decimal(sum_fuel / Decimal(timespan_fuel.days / 365))),
            },
            'other': {
                'total': format_money(Decimal(sum_other)),
                'daily': format_money(Decimal(sum_other / Decimal(timespan_other.days))),
                'weekly': format_money(Decimal(sum_other / Decimal(timespan_other.days / 7))),
                'monthly': format_money(Decimal(sum_other / Decimal(timespan_other.days / 30))),
                'yearly': format_money(Decimal(sum_other / Decimal(timespan_other.days / 365))),
            },
            'total': {
                'total': format_money(Decimal(sum_total)),
                'daily': format_money(Decimal(sum_total / Decimal(timespan_total.days))),
                'weekly': format_money(Decimal(sum_total / Decimal(timespan_total.days / 7))),
                'monthly': format_money(Decimal(sum_total / Decimal(timespan_total.days / 30))),
                'yearly': format_money(Decimal(sum_total / Decimal(timespan_total.days / 365))),
            },
        }

    def format(self):
        """Prepares the data"""
        dfe = []
        for index, row in self.expenses.iterrows():
            match row.recurrence:
                case Other.Recurrence.Never:
                    newobj = row.copy()
                    newobj.date = Timestamp(row.date).date()
                    dfe.append(newobj)
                case Other.Recurrence.Daily:
                    for i in range(0, row.recurrence_iteration_amount):
                        newobj = row.copy()
                        newobj.date = (row.date + DateOffset(days=i)).date()
                        dfe.append(newobj)
                case Other.Recurrence.Weekly:
                    for i in range(0, row.recurrence_iteration_amount):
                        newobj = row.copy()
                        newobj.date = (row.date + DateOffset(weeks=i)).date()
                        dfe.append(newobj)
                case Other.Recurrence.Monthly:
                    for i in range(0, row.recurrence_iteration_amount):
                        newobj = row.copy()
                        newobj.date = (row.date + DateOffset(months=i)).date()
                        dfe.append(newobj)
                case Other.Recurrence.Yearly:
                    for i in range(0, row.recurrence_iteration_amount):
                        newobj = row.copy()
                        newobj.date = (row.date + DateOffset(years=i)).date()
                        dfe.append(newobj)
        self.expenses_extended = DataFrame(dfe)  # dfe.loc([[dfe.date_expensed < datetime.datetime.now()]])
        self.fuel['name'] = 'Fuel'
        self.combined = concat([self.fuel, self.expenses_extended], join="inner", keys=["fuel", "other"])
        self.combined["monthly"] = self.combined.apply((lambda row: f'{row["date"].year}-{row["date"].month}'), axis=1)

        # Return self for function chaining.
        return self

    def total_expenses(self):
        return self.expenses_extended['money'].sum()

    def verify(self) -> list[int]:
        """
        Check that the table is valid.
        :return: A list of indexes which are invalid.
        """
        previous = Fuel(milage=-1, money=-1, liter_price=1)
        invalids = []
        for index, entry in self.fuel.sort_values(by=['date']).iterrows():
            if entry.milage <= previous.milage:
                invalids.append(index)
                break
            previous = entry

        return invalids

    def efficiency(self) -> Decimal:
        """Return the liter/100km that the car runs."""
        total_miles = Decimal(max(self.fuel["milage"]) - min(self.fuel['milage']))
        total_liters = Decimal(self.fuel["fuel_tanked"].sum())
        return Decimal(100 * total_liters / total_miles).quantize(Decimal('.001'))

    def sum_money(self):
        sum = self.fuel["money"].sum()
        timespan = self.fuel["date"].max() - self.fuel["date"].min()
        return sum
