import csv

from decimal import Decimal
from gettext import gettext

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from .Forms.FuelForm import FuelForm
from .Forms.OtherForm import OtherForm
from .format import format_money, format_liter
from .models.fuel import Fuel
from .models.other import Other
from .pictures import Pictures
from .stats import Stats


def index(request):
    s = Stats()
    context = {
        "efficiency": s.efficiency(),
        "distance": Fuel.objects.distance,
        "fuelUsed": format_liter(Fuel.objects.total_fuel()),
        "avgFuelPrice": format_money(Decimal(Fuel.objects.total_money / Fuel.objects.total_fuel())),
        "pricePerkm": format_money(Decimal(s.sum_money() / Fuel.objects.distance * 100)),
        "FuelTable": Fuel.objects,
        "pictures": Pictures(),
        "total_expenses": s.format().summary(),
        "other_summary": Other.objects.summary(),
        "fuel_summary": Fuel.objects.summary(),
        "milage": "Milage",
    }
    return render(request, 'fuel/index.html', context)


def add(request):
    """Form to add a new row to the fuel table."""
    if request.method == "POST":
        # Process the form.
        form = FuelForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect("/fuel/")

    else:
        form = FuelForm().as_table()

    return render(request, "fuel/add.html", {
        "form": form,
        "title": gettext("Add Fuel entry"),
        "endpoint": "/fuel/add-fuel"
    })


def add_others(request):
    """Form to add a new row to the other table."""
    if request.method == "POST":
        form = OtherForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/fuel/")
    else:
        form = OtherForm().as_table()
    return render(request, "fuel/add.html", {
        "form": form,
        "title": gettext("Add other entry"),
        "endpoint": "/fuel/add-other"
    })


def export(request):
    """Returns an export of the fuel table."""
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="fuel_table.csv"'

    writer = csv.writer(response)

    writer.writerow(["obj"] + list(Fuel._meta.get_fields()))

    fuel_table = list(Fuel.objects.all())
    for refuel in fuel_table:
        list_fuel_row = [getattr(refuel, str(x)) for x in vars(refuel)]
        writer.writerow(list_fuel_row)

    return response


def export_other(request):
    """Returns an export of the other table."""
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="other_table.csv"'

    writer = csv.writer(response)
    writer.writerow(["obj"] + list(Other._meta.get_fields()))

    other_table = list(Other.objects.all())
    for refuel in other_table:
        list_fuel_row = [getattr(refuel, str(x)) for x in vars(refuel)]
        writer.writerow(list_fuel_row)

    return response
