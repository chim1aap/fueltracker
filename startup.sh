#!/usr/bin/env sh

# Starts the fueltracker app in a docker context.
echo "Applying Migrations"
python manage.py migrate

# Check if user exists
created=$(python manage.py ensure_adminuser --no-input)
if [ "$created" = "True" ];
then
  echo "Loading fixtures"
  python manage.py loaddata fuel/fixtures/*
fi

python manage.py runserver 0.0.0.0:8000