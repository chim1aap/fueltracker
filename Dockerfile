FROM python:3.11-alpine

# Allows docker to cache installed dependencies between builds
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Mounts the application code to the image
COPY . app
WORKDIR /app

# Generate translations.
RUN apk add --no-cache gettext
RUN python manage.py compilemessages

EXPOSE 8000

# runs the production server
ENTRYPOINT ["sh", "startup.sh"]