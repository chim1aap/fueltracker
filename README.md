# Fueltracker

This application allows you to track the fuel use of your car.
It is a web application using django and plotnine. 


## Run using docker compose
The intended way is to run the docker image. 
Modify the `docker-compose.yaml` file to suit your own needs.
The versions available can be found at the following [link](https://gitlab.com/chim1aap/fueltracker/container_registry/5732729).
I assume that my shoddy programming breaks stuff all the time, so upgrading should be a deliberate process. 

The `latest` tag represents the latest build pushed to gitlab until the software is stabilized sufficiently. 
The stabilization will probably happen around `1.0.0`.

## Run using python
After cloning the project, issue the following commands

```shell
pip install -r requirements.txt
python manage.py compilemessages
python manage.py runserver
```
This will start the webserver at http://localhost:8000. 
For more customization, refer to [the django documentation](https://docs.djangoproject.com/en/4.2/ref/django-admin/).

## Admin credentials
The default credentials are defined by environment variables in the docker-compose file.
Change them if it will be accessible over the internet.

## Adding a language

To add another language, first clone the project, install the dependencies and run the following command (`<lang>` being the two letter language code).

```shell
python manage.py makemessages --ignore venv -l nl
```
This generates the `.po` file for that language.
Next you need to fill in all the `msgstr` entries with the translations for that piece of sentence.

If you are testing with `python manage.py runserver`, you will need to compile the messages.
```shell
python manage.py compilemessages
```
If you are testing with docker, you only need to rebuild the image. 

Finally, once everything looks neat and tidy, create a pull request so that your translation can be included.

[X-Clacks-Overhead Compliant](https://xclacksoverhead.org/home/about)